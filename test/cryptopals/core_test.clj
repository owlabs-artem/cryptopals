(ns cryptopals.core-test
  (:require [clojure.test :refer :all]
            [cryptopals.core :refer :all]))

(deftest hex-to-base64-test
  (testing "Set 1 Challenge 1"
    (is (= (hex-to-base64 "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d")
           "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t"))))

(deftest fixed-xor-test
  (testing "Set 1 Challenge 2"
    (is (=
         (xor-hex-strings "1c0111001f010100061a024b53535009181c"
                          "686974207468652062756c6c277320657965")
         "746865206b696420646f6e277420706c6179"))))

(deftest single-byte-xor-cipher-test
  (testing "Set 1 Challenge 3"
    (is (=
         (decrypt-xored-message "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736")
         "Cooking MC's like a pound of bacon"))))

(deftest find-message-test
  (testing "Set 1 Challenge 4"
    (is (= (find-xored-message challenge-data-1-4)
           "Now that the party is jumping\n"))))

(deftest xor-str-test
  (testing "Set 1 Challenge 5"
    (is (= (bytes-to-hex (xor-str-with-key "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal" "ICE"))
           "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"))))

(deftest bit-distance-test
  (testing "Test Hamming distance between bit strings"
    (is (= (bit-distance "this is a test" "wokka wokka!!!")
           37))))

(deftest base64-roundtrip-test
  (testing "Can encode and decode from base64"
    (let [full-circle (comp hex-to-base64 bytes-to-hex base64-to-bytes)]
      (is (= "abcd" (full-circle "abcd")))
      (is (= "abcdefgh" (full-circle "abcdefgh")))
      ;(is (= "abcdefg=" (full-circle "abcdefg"))) ; doesn't work
      (is (= "" (full-circle ""))))))

(deftest challenge-1-6-test
  (testing "Key for Set 1 Challenge 6 is found correctly"
    (is (= (get-challenge-1-6-key) "Terminator X: Bring the noise"))))
