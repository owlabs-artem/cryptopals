(defproject cryptopals "0.1.0-SNAPSHOT"
  :license {:name "WTFPL"
            :url "http://www.wtfpl.net/"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [clj-http "3.7.0"]
                 [com.clojure-goes-fast/clj-async-profiler "0.1.0"]
                 [criterium "0.4.4"]]
  :main ^:skip-aot cryptopals.core
  :target-path "target/%s"
  :jvm-opts ^:replace ["-server"]
  :plugins [[lein-nodisassemble "0.1.3"]]
  :profiles {:uberjar {:aot :all}})
