(ns cryptopals.core
  (:gen-class)
  (:require [clj-http.client :as client])
  (:require [clojure.string :as str]))

(set! *unchecked-math* true)

(def ^:const challenge-data-1-4
  (:body (client/get "https://cryptopals.com/static/challenge-data/4.txt")))

(defn char-range
  [start end]
  (map char (range (int start) (-> end int inc))))

(def ^:const base64table
  (let* [lowercase (char-range \a \z)
         uppercase (char-range \A \Z)
         numbers (char-range \0 \9)
         b64seq (concat uppercase lowercase numbers [\+ \/])]
         (vec b64seq)))

(defn char<= [a b] (<= (int a) (int b)))
(defn char- [a b] (- (int a) (int b)))

(defn decode-b64-char [c]
  (cond
    (and (char<= \A c) (char<= c \Z)) (char- c \A)
    (and (char<= \a c) (char<= c \z)) (+ 26 (char- c \a))
    (and (char<= \0 c) (char<= c \9)) (+ 52 (char- c \0))
    (= c \+) 62
    (= c \/) 63
    (= c \=) 0))

(defn b64-seq-to-int [b64]
  (->> b64 (map decode-b64-char) (reduce #(+ %2 (* 64 %1)))))

(defn decode-b64-quadruple
  [a b c d]
  (let [output-length (cond (= c \=) 1, (= d \=) 2, true 3)
        n (b64-seq-to-int [a b c d])
        output-bytes [(bit-shift-right n 16)
                      (bit-and 0xFF (bit-shift-right n 8))
                      (bit-and 0xFF n)]]
        (take output-length output-bytes)))

(defn base64-to-bytes [str]
  (->> str
       (partition 4)
       (mapcat (partial apply decode-b64-quadruple))))

(def ^:const challenge-data-1-6
  (base64-to-bytes
   (str/replace
    (:body (client/get "https://cryptopals.com/static/challenge-data/6.txt"))
    #"\n"
    "")))

(defn hex-to-nibbles
  [hex-string]
  (map #(Character/digit ^char % 16) hex-string))

(defn hex-to-bytes
  [hex-string]
  (->> hex-string
       hex-to-nibbles
       (partition 2 2 [0])
       (map (partial apply #(+ %2 (* 16 %1))))))

(def nibbles-to-hex
  #(->> %
        (map (partial format "%h"))
        (apply str)))

(defn hex-triple-to-base64
  [h1 h2 h3]
  (let [num (+ h3 (* 16 (+ h2 (* 16 h1))))]
    [(quot num 64) (rem num 64)]))

(def hex-to-base64
  #(->> %
        hex-to-nibbles
        vec
        (partition 3 3 [0 0])
        (mapcat (partial apply hex-triple-to-base64))
        (map base64table)
        (apply str)))

(defn xor-hex-strings [h1 h2]
  (nibbles-to-hex (map bit-xor (hex-to-nibbles h1) (hex-to-nibbles h2))))

(defn bytes-to-str [bytes]
  (String. (byte-array bytes)))

(defn bytes-to-hex [bytes]
  (->> bytes
       (mapcat (fn [n] [(quot n 16) (rem n 16)]))
       nibbles-to-hex))

#_(defn xor-with-key [bytes key]
  (byte-array (map bit-xor bytes (cycle key))))

(defn xor-with-key ^ints [^ints bytes ^ints key]
  (let [input-length (alength bytes)
        key-length (alength key)
        result (int-array input-length)]
    (loop [i 0]
      (if (= i input-length)
        result
        (do
          (aset result i
                (int (bit-xor
                  (aget ^ints bytes i)
                  (aget ^ints key (rem i key-length)))))
          (recur (unchecked-inc i) ))))))

(defn get-bytes [str] (int-array (map int (.getBytes ^String str))))

(defn xor-str-with-key [str key]
  (xor-with-key (get-bytes str) (get-bytes key)))

(defn square [x]
  (* x x))

(defn printable? [^long letter]
  (let [n letter]
    (or
     (= n 9) ; allow tabs
     (= n 10) ; allow newline characters
     (= n 13)
     (and
      (>= n 32)
      (< n 127)))))

(defn score-sentence [s]
  (->> s (re-seq #"[ \neta]") count))

(defn xor-with-character
  [text char]
  (->> [char] int-array (xor-with-key (int-array text))))

(defn ints-to-str [int-seq]
  (apply str (map char int-seq)))

#_(defn score-int-sequence [s]
  (if (every? printable? s)
    (score-sentence (ints-to-str s))
    0))

(defn score-int-sequence [s]
  (score-sentence (ints-to-str s)))

(defn find-single-byte-key
  [byte-seq]
  (let [b (int-array byte-seq)
        score #(score-int-sequence (xor-with-character b %))]
    (apply max-key score (range 0 256))))

(defn decrypt-xored-bytes
  [byte-seq]
  (ints-to-str (xor-with-character (int-array byte-seq) (find-single-byte-key byte-seq))))

(defn decrypt-xored-message
  [hex-string]
  (decrypt-xored-bytes (hex-to-bytes hex-string)))

(defn find-xored-message
  [text]
  (->> text
       str/split-lines
       (pmap decrypt-xored-message)
       (apply max-key score-sentence)))

(defn bit-distance [s1 s2]
  (reduce + (map (comp #(Integer/bitCount %) bit-xor)
                 (get-bytes s1)
                 (get-bytes s2))))

(defn normalized-bit-distance
  [s1 s2]
  (float (/ (bit-distance s1 s2) (count s1))))

(defn mean [seq] (float (/ (reduce + seq) (count seq))))

(defn average-normalized-bit-distance
  [text key-length]
  (->> text
       (partition key-length)
       (map #(apply str %))
       (partition 2)
       (map #(apply normalized-bit-distance %))
       (mean)))

(defn check-key-lengths
  [text min-length max-length]
  (let [score (fn [n] (average-normalized-bit-distance text n))
        lengths (range min-length max-length)]
    (do
      (doseq [n lengths]
        (println n (score n)))
      (println "Best key length:"
               (apply min-key score lengths)))))

(defn partition-text
  [block-size text]
  (partition block-size block-size (repeat block-size (int \space)) text))

(defn transpose-bytes
  [text block-size]
  (let [blocks (into [] (map vec (partition-text block-size text)))
        trans (fn [i] (map #(get % i) blocks))]
    (map trans (range 0 block-size))))


(defn reverse-transpose-bytes
  [text block-size]
  (transpose-bytes text
                   (int (Math/ceil (/ (count text) block-size)))))


; what's proposed on the website (finding hamming distance between bit sequences)
; doesn't seem to work as advertised, so what I do instead is just go through possible key lengths
; and then through all possible key bytes and just dump anything printable to the console
; (sample-size of 100 leaves very few options already)
(defn find-key-length [text sample-size]
  (doseq [key-length (range 2 40)]
    (let [sample
          (->> (-> text (transpose-bytes key-length) first)
               (map int)
               (take sample-size))]
      (doseq [ch (range 0 256)]
        (let [s (xor-with-character sample ch)]
          (if (every? printable? s)
            (let [decoded (ints-to-str s)]
              (println "Key length:" key-length
                       "\n\tKey:" ch
                       "\n\t" decoded
                       "\n\tScore:" (score-sentence decoded)))))))))

(defn get-challenge-1-6-key []
  (ints-to-str (map find-single-byte-key (transpose-bytes challenge-data-1-6 29))))

; https://en.wikipedia.org/wiki/Play_That_Funky_Music#Vanilla_Ice_version
(defn print-challenge-1-6-solution
  []
  (->> (get-challenge-1-6-key)
       (map int)
       (int-array)
       (xor-with-key (int-array challenge-data-1-6))
       (ints-to-str)
       (println)))

(import 'javax.crypto.Cipher)
(import 'javax.crypto.spec.SecretKeySpec)

(defn decrypt-aes
  [bytes key]
  (let [secret-key (SecretKeySpec. (.getBytes key) "AES")
        cipher (Cipher/getInstance "AES/ECB/PKCS5Padding")]
    (do
      (.init cipher Cipher/DECRYPT_MODE secret-key)
      (.doFinal cipher (byte-array bytes)))))

(defn decrypt-aes-b64 [text key]
  (decrypt-aes (base64-to-bytes (str/replace text #"\n" "")) key))

; challenge 8 - run the code below, then look for it in the file
; both repeating 16-byte blocks come from the same line
#_(doseq [[k v] (frequencies
               (partition 16
                          (str/replace (slurp "8.txt") #"\n" "")))]
  (if (> v 1)
    (println k v)))


(defn -main
  [& args]
  (println "Hello, Cryptopals!"))
